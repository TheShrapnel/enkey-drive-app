let colors = require('colors');
class ConsoleLogger{
    constructor(){

    }

    logInfo(message){
        console.log(`Info:  ${(new Date()).toLocaleString()}   ${message}`.green);
    }

    logError(message){
        console.log(`Error: ${(new Date()).toLocaleString()}   ${message}`.red);
    }

    logWarning(message){
        console.log(`Warning: ${(new Date()).toLocaleString()}   ${message}`.yellow);
    }
}

module.exports = new ConsoleLogger();
let settings = require('./../../settings')(process.env.NODE_ENV);
let mysql = require('mysql');
let crypto = require('crypto');

class UserRepository {
    constructor() {
        // console.log(settings);
        this.con = mysql.createConnection(settings.db.connection);
        // console.log(this.con);
    }

    getAll(success, error) {
        let stringQuery = `SELECT * FROM ${settings.db.tables.user}`;
        this.con.query(stringQuery, (err, results, fields) => {
            if (err)
                error();
            success(results);
        });
    };
    getById(params, success, error) {
        let stringQuery = `SELECT * FROM ${settings.db.tables.user} WHERE Id=${params.id}`;
        this.con.query(stringQuery, (err, result, fields) => {
            if (err || !result.length)
                error();
            success(result.pop());
        });
    };
    add(params, success, error) {
        params.password= crypto.createHash('md5').update(params.password).digest('hex');

        let stringQuery = `INSERT INTO ${settings.db.tables.user} (name, password, token, remember_token, email, created_at, created_by, updated_at, updated_by)
         VALUES ('${params.name}', '${params.password}', '', '', '${params.email}', CURRENT_TIMESTAMP, '0', NULL, NULL);`;
        this.con.query(stringQuery, (err, result) => {
            if (err)
                error();
            success(true);
        });
    };
};
module.exports = new UserRepository();
let settings = require('./../../settings')(process.env.NODE_ENV);
let mysql = require('mysql');

class AnalyticsRepository{
    constructor(){
        this.con = mysql.createConnection(settings.db.connection);
    }
    getAll(success, error) {
        let stringQuery = `SELECT * FROM ${settings.db.tables.analytics}`;
        this.con.query(stringQuery, (err, results, fields) => {
            if (err)
                error();
            success(results);
        });
    };
};
module.exports = new AnalyticsRepository();
let settings = {
    development: {
        mode: 'development',
        port: 3000,
        db: {
            connection : {
                host : "127.0.0.1",
                // port     : '3306',
                database : "enkeydrive",
                user : "root",
                password : ""
            },
            tables : {
                analytics : "analytics",
                file : "file",
                folder : "folder",
                key : "key",
                user : "user"
            }
        }
    },
    staging: {
        mode: 'staging',
        port: 4000,
        db: {
            connection : {
                host : "localhost",
                port     : '3306',
                database : "enkeydrive",
                user : "root",
                password : ""
            },
            tables : {
                analytics : "analytics",
                file : "file",
                folder : "folder",
                key : "key",
                user : "user"
            }
        }
    },
    production: {
        mode: 'production',
        port: 4444,
        db: {
            connection : {
                host : "89.40.0.39",
                // port     : '3306',
                database : "EnkeyDrive",
                user : "root",
                password : "nopass"
            },
            tables : {
                analytics : "analytics",
                file : "file",
                folder : "folder",
                key : "key",
                user : "user"
            }
        }
    }
}
module.exports = function(mode) {
    return settings[mode || process.argv[2] || 'development'] || settings.development;
}
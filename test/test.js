const request = require('supertest');

 
var app = require('../server.js')
  
 describe('GET /home/index', function() {
   it('checks if the first page is obtained', function(done) {
     // The line below is the core test of our app.
     request(app).get('/home/index').expect(200,done);
   });
 });

 describe('GET /', function() {
  it('checks if the redirection works', function(done) {
    // The line below is the core test of our app.
    request(app).get('/').expect(302,done);
  });
});
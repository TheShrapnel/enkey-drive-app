let express = require('express');
let STATUS_CODES = require('./../enums/status-codes');
let logger = require('./../services/console-logger');

let HomeController = require('./../controllers/home');
let UserController = require('./../controllers/api/user');
let AnalyticsController = require('./../controllers/api/analytics');

let router = express.Router();



router.get('/', function (req, res) {
    res.redirect("/home/dashboard");
});



router.all('/api/:controller([a-z]+)/:action(*)/:id(*)', (req, res) => {
    var Controller;
    switch (req.params.controller) {
        case "analytics":
            Controller = AnalyticsController;
            logger.logInfo(`req:${req.params.controller}    case:analytics`);
            break;
        case "user":
            Controller = UserController;
            logger.logInfo(`req:${req.params.controller}    case:user`);
            break;
    }

    try {
        switch (req.method) {
            case "GET":
                {
                    if (typeof (Controller) != "undefined" || typeof (Controller[req.params.action]) != "undefined") {
                        let action = req.params.action;
                        delete req.params.action;
                        delete req.params.controller;
                        delete 0; delete 1;
                        if (req.params.id === "" && req.body === null)
                            Controller.get[action](req, res);
                        else if (Object.keys(req.body).length)
                            Controller.get[action](req, res, req.body);
                        else
                            Controller.get[action](req, res, req.params);
                    }
                    else
                        res.status(STATUS_CODES.NOT_FOUND).send(JSON.stringify({ "status": STATUS_CODES.NOT_FOUND, "error": "Not Found" }));
                }
                break;
            case "POST":
                {
                    if (typeof (Controller) != "undefined" || typeof (Controller[req.params.action]) != "undefined") {
                        let action = req.params.action;
                        if (Object.keys(req.body).length)
                            Controller.post[action](req, res, req.body);
                        else
                            res.status(STATUS_CODES.BAD_REQUEST).send(JSON.stringify({ "status": STATUS_CODES.BAD_REQUEST, "error": "Body is empty" }));
                    }
                    else
                        res.status(STATUS_CODES.METHOD_NOT_ALLOWED).send(JSON.stringify({ "status": STATUS_CODES.METHOD_NOT_ALLOWED, "error": "Method not allowed" }));
                }
                break;
            case "PUT":
                res.status(STATUS_CODES.NOT_IMPLEMENTED).send(JSON.stringify({ "status": STATUS_CODES.NOT_IMPLEMENTED, "error": "Method not implemented" }));
                break;
            case "DELETE":
                res.status(STATUS_CODES.NOT_IMPLEMENTED).send(JSON.stringify({ "status": STATUS_CODES.NOT_IMPLEMENTED, "error": "Method not implemented" }));
                break;
            default:
                res.status(STATUS_CODES.METHOD_NOT_ALLOWED).send(JSON.stringify({ "status": STATUS_CODES.METHOD_NOT_ALLOWED, "error": "Method not allowed" }));
                break;

        }
    }
    catch(err){
        logger.logError(`${err.name} ${err.message}`);
        res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send(JSON.stringify({ "status": STATUS_CODES.INTERNAL_SERVER_ERROR, "error": "Server Error" }));
    }



});

router.get('/:controller([a-z]+)/:action(*)', (req, res) => {
    var Controller;
    switch (req.params.controller) {
        case "home":
            Controller = HomeController;
            logger.logInfo(`req:${req.params.controller}    case:home`);
            break;
        default:
            logger.logInfo(`req:${req.params.controller}    case:default`);
            Controller = HomeController;
            break;
    }

    try {
        if (typeof (Controller[req.params.action]) != "undefined")
            Controller[req.params.action](req, res);
        else if (req.params.action == "")
            res.redirect(Controller["default"]);
        else
            res.render("shared/error", { code: STATUS_CODES.NOT_FOUND, message: "Page Not Found" });
    }
    catch(err){
        logger.logError(`${err.name} ${err.message}`);
        res.render("shared/error", { code: STATUS_CODES.INTERNAL_SERVER_ERROR, message: "Server Error" });
    }

});

router.get('*', (req, res) => {
    logger.logWarning(`uri requested: ${req.url}`);
    res.render("shared/error", { code: STATUS_CODES.NOT_FOUND, message: "Page Not Found" });
});

module.exports = router;
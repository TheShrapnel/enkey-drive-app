//Imports
const express = require('express');
const bodyParser = require('body-parser');
const path = require("path");
const upload = require('multer')();
const settings = require("./settings")();
require('dotenv').config();

//Setting up app
var app = express();


//Routes
let routes = require('./routes/route');


//all library assets
app.use("/lib",express.static(path.join(__dirname,"node_modules")));

//all js and css
app.use("/public",express.static(path.join(__dirname,"public")));



// Template Engine
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

//urlencoded
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());
app.use(bodyParser.raw({ inflate: true, limit: '1000kb', type: 'text/xml' }));

// for parsing multipart/form-data
app.use(upload.array()); 

app.use(routes);

const server  = app.listen(process.env.PORT || settings.port,()=>{
     var host = server.address().host || "localhost";
     var port = server.address().port;
    console.log(`Server listening on Port http://${host}:${port}`);
});

module.exports = app;
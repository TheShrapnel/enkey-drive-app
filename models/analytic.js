class Analytic {
    constructor(id,algo,file_size,enct_time,dect_time,throughput){
        this.id = id;
        this.algo = algo;
        this.file_size = file_size;
        this.enct_time = enct_time;
        this.dect_time = dect_time;
        this.throughput = throughput;
    }
}

module.exports = Analytic;
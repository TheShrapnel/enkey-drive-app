let settings = require('./../settings')(process.env.NODE_ENV);

var HomeController = function(){
    return {
        dashboard : function(req,res){
            res.render('home/dashboard', {
                title : "Dashboard"
            });
        },
        analytics : function(req,res){
            
            res.render('home/analytics', {
                title : "Analytics"
            });
        },
        index : function(req,res){
            res.render('home/index', {
                title : "Login"
            });
        },
        default : "index"
    }
}();

module.exports = HomeController;
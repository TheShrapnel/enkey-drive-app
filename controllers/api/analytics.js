let STATUS_CODES = require('./../../enums/status-codes');
let analyticsRepo = require('./../../services/repositories/analytics-repo');

var AnalyticsController = 
function(){
    return {
        get : {
            all : function(req,res){
                analyticsRepo.getAll((result)=>{
                    res.status(STATUS_CODES.OK).send(JSON.stringify({"status": STATUS_CODES.OK, "error": null, "response": result}));
                },()=>{
                    res.status(STATUS_CODES.NOT_FOUND).send(JSON.stringify({"status": STATUS_CODES.NOT_FOUND, "error": "Not Found"}));
                });
            },
        },
        post : {
            
        },
        put : {},
        delete : {}
    }
}();

module.exports = AnalyticsController;
let userRepo = require('./../../services/repositories/user-repo');
let STATUS_CODES = require('./../../enums/status-codes');

var UserController = 
function(){
    return {
        get : {
            all : function(req,res){
                userRepo.getAll((result)=>{
                    res.status(STATUS_CODES.OK).send(JSON.stringify({"status": STATUS_CODES.OK, "error": null, "response": result}));
                },()=>{
                    res.status(STATUS_CODES.NOT_FOUND).send(JSON.stringify({"status": STATUS_CODES.NOT_FOUND, "error": "Not Found"}));
                });
            },
            byId : function(req,res,params){
                userRepo.getById(params,(result)=>{
                    res.status(STATUS_CODES.OK).send(JSON.stringify({"status": STATUS_CODES.OK, "error": null, "response": result}));
                },()=>{
                    res.status(STATUS_CODES.NOT_FOUND).send(JSON.stringify({"status": STATUS_CODES.NOT_FOUND, "error": "Not Found"}));
                });
            }
        },
        post : {
            new : function(req,res,params){
                userRepo.add(params,(confirm)=>{
                    if (confirm)
                    res.status(STATUS_CODES.CREATED).send(JSON.stringify({"status": STATUS_CODES.CREATED, "error": null}));
                },()=>{
                    res.status(STATUS_CODES.BAD_REQUEST).send(JSON.stringify({"status": STATUS_CODES.BAD_REQUEST, "error": "Bad Request"}));
                });
            }
        },
        put : {},
        delete : {}
    }
}();

module.exports = UserController;